const assert = require("assert")

const config = require("../config/config")
const loggerLog = config.logger

var logger = require("tracer").console()

const database = require("../config/database")
const pool = require("../config/database")

const databaseExecutor = require("./sqlExecute")
const sqlMealtimeDao = require("./sqlMealtimeDao")

module.exports = {
	// Every DAO calls a method from sqlExecute.js
	// Every sqlDAO is responsible for making the SQL query
	createHome(req, res, errorMessage, errorStatus) {
		// Every create function has its method in the dao because of what needs to happen on line 38
		const studenthome = req.body
		let { houseName, address, houseNr, postalCode, phoneNumber, cityName } =
			studenthome

		console.log(req.userId)

		const sqlQuery =
			"INSERT INTO `studenthome`(`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES (?,?,?,?,?,?,?)"

		pool.getConnection(function (err, connection) {
			if (err) {
				logger.error("createMovie", error)
				res.status(400).json({
					message: "createMovie failed getting connection!",
					error: err,
				})
			}
			if (connection) {
				// This code \/
				connection.query(
					sqlQuery,
					[
						houseName,
						address,
						houseNr,
						req.userId,
						postalCode,
						phoneNumber,
						cityName,
					],
					(error, results, fields) => {
						// When done with the connection, release it.
						connection.release()
						// Handle error after the release.
						if (error) {
							logger.error("createStudenthome", error.toString())
							res.status(errorStatus).json({
								message: errorMessage,
								error: error.toString(),
							})
						}
						if (results) {
							logger.trace("results: ", results)

							res.status(200).json({
								result: {
									id: results.insertId,
									...studenthome,
								},
							})
						}
					}
				)
			}
		})
	},

	deleteById(req, res, homeId, errorMessage, errorStatus) {
		
		// Every query that needs authority to be checked (delete and update) have extra queries
		const sqlQuery =
			// First delete all meals that belong to studenthome, else studenthome can't be deleted
			"DELETE " +
			"FROM meal" +
			" WHERE StudentHomeID = " +
			homeId +
			// check if the user can delete the meal
			" AND UserID = " +
			req.userId +
			";" +
			// delete the studenthome
			"DELETE " +
			"FROM studenthome" +
			" WHERE ID = " +
			homeId +
			// again check if the user can delete
			" AND UserID = " +
			req.userId +
			"; " +
			// query to check if the meal exists. This is because if the above queries fail, it could be because of 2 reasons:
			// 1: the home that is searched for can not be found
			// 2: the user doesn't the rights to delete the home
			// Because of this we have to add a query to check if the to be deleted house exists, so we can rule this out. (more on this in sqlExecute)
			"SELECT * FROM studenthome WHERE ID = " +
			homeId +
			";"

		pool.getConnection(function (err, connection) {
			databaseExecutor.executeDeleteStudenthomeQuery(
				err,
				connection,
				res,
				sqlQuery,
				errorMessage,
				errorStatus
			)
		})
	},

	getById(req, res, homeId, errorMessage, errorStatus) {
		const sqlQuery =
			"SELECT * " + "FROM studenthome " + " WHERE ID = " + homeId

		pool.getConnection(function (err, connection) {
			databaseExecutor.executeQuery(
				err,
				connection,
				res,
				sqlQuery,
				errorMessage,
				errorStatus
			)
		})
	},

	getByCity(req, res, errorMessage, errorStatus) {
		const cityName = req.query.city
		const houseName = req.query.name

		const sqlQuery =
			"SELECT * " +
			"FROM studenthome " +
			" WHERE City = " +
			'"' +
			cityName +
			'"' +
			" AND Name = " +
			'"' +
			houseName +
			'"'

		pool.getConnection(function (err, connection) {
			databaseExecutor.executeQuery(
				err,
				connection,
				res,
				sqlQuery,
				errorMessage,
				errorStatus
			)
		})
	},

	changeHomeById(req, res, errorMessage, errorStatus) {
		const homeId = req.params.homeId

		const sqlQuery =
			"UPDATE studenthome " +
			"SET " +
			// ID = " +
			// homeId +
			" Name = " +
			'"' +
			req.body.houseName +
			'"' +
			", Address = " +
			'"' +
			req.body.adress +
			'"' +
			", House_Nr = " +
			req.body.houseNr +
			", UserId = " +
			req.userId +
			", Postal_Code = " +
			'"' +
			req.body.postalCode +
			'"' +
			", Telephone = " +
			'"' +
			req.body.phoneNumber +
			'"' +
			", City = " +
			'"' +
			req.body.cityName +
			'"' +
			" WHERE ID = " +
			homeId +
			// Once again checking if the user has the rights to change the home
			" AND UserID = " +
			req.userId +
			"; " +
			// Query to check if meal exists
			"SELECT * FROM studenthome WHERE ID = " +
			homeId +
			";"

		pool.getConnection(function (err, connection) {
			databaseExecutor.executeUpdateQuery(
				err,
				connection,
				res,
				sqlQuery,
				errorMessage,
				errorStatus
			)
		})
	},
}
