var logger = require("tracer").console()

const express = require("express")
const mealtimeController = require("../controllers/mealtimeController")
const router = express.Router()
const authcontroller = require("../controllers/authentication.controller")
logger.log("Called class mealtime")

// UC 301 - 305

// 301
router.post(
	"/:homeId/meal/",
	authcontroller.validateToken,
	mealtimeController.validateMeal,
	mealtimeController.createMeal
)
// 302
router.put(
	"/:homeId/meal/:mealId",
	authcontroller.validateToken,
	mealtimeController.validateMeal,
	mealtimeController.changeMeal
)
// 303
router.get("/:homeId/meal/", mealtimeController.findMealByHomeId)
// 304
router.get("/:homeId/meal/:mealId", mealtimeController.findMealByMealId)
//305
router.delete(
	"/:homeId/meal/:mealId",
	authcontroller.validateToken,
	mealtimeController.deleteMeal
)

module.exports = router
