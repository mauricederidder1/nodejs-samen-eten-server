var createError = require("http-errors")
var express = require("express")
var path = require("path")
var cookieParser = require("cookie-parser")
var logger = require("morgan")

var indexRouter = require("./src/routes/index")
var usersRouter = require("./src/routes/users")
var studenthomeRouter = require("./src/routes/studenthome")
var mealRouter = require("./src/routes/mealtime")
const authroutes = require("./src/routes/authentication.routes")

var app = express()

// view engine setup
app.set("views", path.join(__dirname, "views"))
app.set("view engine", "pug")

app.use(logger("dev"))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, "public")))

app.use("/", indexRouter)
app.use("/users", usersRouter)
app.use("/api/studenthome", studenthomeRouter)
app.use("/api/studenthome", mealRouter)
app.use("/api", authroutes)
// use case 103
app.get("/api/info", function (req, res) {
	const creator = {
		name: "Maurice de Ridder",
		studentNumber: "2168234",
		description: "Studentenhuis app",
	}
	res.status(200).json(creator)
})

// catch 404 and forward to error handler
app.put(function (req, res, next) {
	next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message
	res.locals.error = req.app.get("env") === "development" ? err : {}

	// render the error page
	res.status(err.status || 500)
	res.render("error")
})

module.exports = app
