const chai = require("chai")
const chaiHttp = require("chai-http")
const server = require("../../app")
const jwt = require("jsonwebtoken")
const assert = require("assert")
const pool = require("../../src/config/database")
const logger = require("../../src/config/config").logger
chai.should()
chai.use(chaiHttp)

const INSERT_MEALTIME =
	"INSERT INTO `meal` (`ID`, `Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES" +
	"('1', 'TEST_NAME', 'TEST_DESCRIPTION', 'TEST_INGREDIENTS', 'TEST_ALLERGIES', '2021-05-17 11:23:18', '2021-05-17 11:23:18', '2', '1', '1', '5')," +
	"('2', 'TEST_NAME', 'TEST_DESCRIPTION', 'TEST_INGREDIENTS', 'TEST_ALLERGIES', '2021-05-17 11:23:18', '2021-05-17 11:23:18', '3', '2', '1', '5')," +
	"('4', 'TEST_NAME', 'TEST_DESCRIPTION', 'TEST_INGREDIENTS', 'TEST_ALLERGIES', '2021-05-17 11:23:18', '2021-05-17 11:23:18', '3', '2', '4', '5')," +
	"('3', 'TEST_NAME', 'TEST_DESCRIPTION', 'TEST_INGREDIENTS', 'TEST_ALLERGIES', '2021-05-17 11:23:18', '2021-05-17 11:23:18', '3', '2', '4', '5');" 


const INSERT_STUDENTHOMES =
	"INSERT INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES " +
	"(1, 'TEST_NAME', 'TEST_ADRESS', '1', '1', 'TEST_POSTCODE1', '0681171454', 'Breda')," +
	"(2, 'TEST_NAME', 'TEST_ADRESS', '1', '2', 'TEST_POSTCODE2', '0681171454', 'Breda')," +
	"(4, 'TEST_NAME_DIFFERENT', 'TEST_ADRESS_DIFFERENNT', '1', '2', 'TEST_POSTCODE4', '0681171454', 'Breda')," +
	"(3, 'OTHER_TEST_NAME', 'TEST_ADRESS', '1', '1', 'TEST_POSTCODE3', '0681171454', 'Tilburg');"

describe("Mealtime", function () {
	before((done) => {
		pool.query(INSERT_STUDENTHOMES, (err, rows, fields) => {
			if (err) {
				logger.error(`before studenthome queries: ${err}`)
				done(err)
			}
			if (rows) {
				logger.debug(`before studenthome queries`)
				done()
			}
		})
	})

	before((done) => {
		pool.query(INSERT_MEALTIME, (err, rows, fields) => {
			if (err) {
				logger.error(`before mealtime queries: ${err}`)
				done(err)
			}
			if (rows) {
				logger.debug(`before mealtime queries`)
				done()
			}
		})
	})
	// 301
	describe("post", function () {
		it("TC-301-1 Mandatory field missing", (done) => {
			chai.request(server)
				.post("/api/studenthome/2/meal")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					// name missing
					description: "TEST_CREATE_DESCRIPTION",
					ingredients: "TEST_CREATE_INGREDIENTS",
					allergies: "TEST_CREATE_ALLERGIES",
					price: 1,
					maxParticipants: 100,
				})
				.end((err, res) => {
					res.should.have.status(400)
					res.body.should.be.a('object')
					done()
				})
		})
	})
	describe("post", function () {
		it("TC-301-2 not logged in", (done) => {
			chai.request(server)
				.post("/api/studenthome/3/meal")
				// no auth sent
				.send({
					name: "TEST_CREATE_NAME",
					description: "TEST_CREATE_DESCRIPTION",
					ingredients: "TEST_CREATE_INGREDIENTS",
					allergies: "TEST_CREATE_ALLERGIES",
					price: 1,
					maxParticipants: 100,
				})
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a('object')
					done()
				})
		})
	})
	describe("post", function () {
		it("TC-301-3 Meal added succesfully", (done) => {
			chai.request(server)
				.post("/api/studenthome/3/meal")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					name: "TEST_CREATE_NAME",
					description: "TEST_CREATE_DESCRIPTION",
					ingredients: "TEST_CREATE_INGREDIENTS",
					allergies: "TEST_CREATE_ALLERGIES",
					price: 1,
					maxParticipants: 100,
				})
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a('object')
					done()
				})
		})
	})
	// 302
	describe("put", function () {
		it("TC-302-1 Mandatory field missing", (done) => {
			chai.request(server)
				.put("/api/studenthome/2/meal/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					// name missing
					description: "TEST_CREATE_DESCRIPTION",
					ingredients: "TEST_CREATE_INGREDIENTS",
					allergies: "TEST_CREATE_ALLERGIES",
					price: 1,
					maxParticipants: 100,
				})
				.end((err, res) => {
					res.should.have.status(400)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	describe("put", function () {
		it("TC-302-2 Not logged in", (done) => {
			chai.request(server)
				.put("/api/studenthome/2/meal/1")
				.send({
					name: "TEST_CREATE_NAME",
					description: "TEST_CREATE_DESCRIPTION",
					ingredients: "TEST_CREATE_INGREDIENTS",
					allergies: "TEST_CREATE_ALLERGIES",
					price: 1,
					maxParticipants: 100,
				})
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	describe("put", function () {
		it("TC-302-3 Not the owner of the data", (done) => {
			chai.request(server)
				.put("/api/studenthome/4/meal/4")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
	            .send({
	                name : "TEST_CREATE_NAME",
	                description : "TEST_CREATE_DESCRIPTION",
	                ingredients : "TEST_CREATE_INGREDIENTS",
	                allergies : "TEST_CREATE_ALLERGIES",
	                price : 1,
	                maxParticipants : 100
				})
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	describe("put", function () {
		it("TC-302-4 Meal doesn't exist", (done) => {
			chai.request(server)
				.put("/api/studenthome/2/meal/5")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					name: "TEST_CREATE_NAME",
					description: "TEST_CREATE_DESCRIPTION",
					ingredients: "TEST_CREATE_INGREDIENTS",
					allergies: "TEST_CREATE_ALLERGIES",
					price: 1,
					maxParticipants: 100,
				})
				.end((err, res) => {
					res.should.have.status(404)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	describe("put", function () {
		it("TC-302-5 Meal succesfully updated", (done) => {
			chai.request(server)
				.put("/api/studenthome/1/meal/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.send({
					name: "TEST_PUT_NAME",
					description: "TEST_PUT_DESCRIPTION",
					ingredients: "TEST_PUT_INGREDIENTS",
					allergies: "TEST_PUT_ALLERGIES",
					price: 1,
					maxParticipants: 100,
				})
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	// 303

	describe("get", function () {
		it("TC-303-1 Meals returned", (done) => {
			chai.request(server)
				.get("/api/studenthome/1/meal")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	// 304

	describe("get", function () {
		it("TC-304-1 Meal doesn't exist", (done) => {
			chai.request(server)
				.get("/api/studenthome/1/meal/5")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(404)
					res.body.should.be.a('object')
					done()
				})
		})
	})
	describe("get", function () {
		it("TC-304-2 details returned", (done) => {
			chai.request(server)
				.get("/api/studenthome/1/meal/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	// 305

	// 305-1 hoeft niet

	// 305-2
	describe("delete", function () {
		it("TC-305-2 not logged in", (done) => {
			chai.request(server)
				.delete("/api/studenthome/1/meal/1")
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a('object')
					done()
				})
		})
	})
	
	describe("delete", function () {
		it("TC-305-2 not authorised to delete", (done) => {
			chai.request(server)
				.delete("/api/studenthome/4/meal/3")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(401)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	describe("delete", function () {
		it("TC-305-3 meal does not exist", (done) => {
			chai.request(server)
				.delete("/api/studenthome/1/meal/99999")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(404)
					res.body.should.be.a('object')
					done()
				})
		})
	})

	describe("delete", function () {
		it("TC-305-4 deleted succesfully", (done) => {
			chai.request(server)
				.delete("/api/studenthome/1/meal/1")
				.set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
				.end((err, res) => {
					res.should.have.status(200)
					res.body.should.be.a('object')
					done()
				})
		})
	})
})
