const assert = require("assert")

const config = require("../config/config")
const loggerLog = config.logger

var logger = require("tracer").console()
const database = require("../config/database")
const pool = require("../config/database")

const error_statuses = require("../controllers/error_statuses")

module.exports = {
	// this is for the get functions
	executeQuery(err, connection, res, sqlQuery, errorMessage, errorStatus) {
		logger.log("Called executeQuery")
		if (err) {
			res.status(errorStatus).json({
				message: errorMessage,
				error: err,
			})
		}
		if (connection) {
			connection.query(sqlQuery, (error, results, fields) => {
				connection.release()
				if (error) {
					res.status(errorStatus).json({
						message: errorMessage,
						error: error,
					})
				}
				if (results) {
					loggerLog.trace("results: ", results)
					// if getting
					try {
						// if getting
						const mappedResults = results.map((item) => {
							return {
								...item,
								user: {
									name:
										item.First_Name + " " + item.Last_Name,
								},
							}
						})
						try {
		
							if (results[0].affectedRows == 0) {
								// This is the case when no rows are deleted in studenthome.delete
								// if no results are found, return 404
								res.status(error_statuses.STATUS_404).json({
									result: "No results found/impacted",
								})
							} else {
								// this is the case when everything just works
								res.status(200).json({
									result: mappedResults,
								})
							}
						} catch (err) {
							// this is the case when delete or update works
							if (mappedResults.length > 0) {
								res.status(200).json({
									result: mappedResults,
								})
							} else {
								// if no results are found, return 404
								res.status(error_statuses.STATUS_404).json({
									result: "None found or you do not have acces",
								})
							}
						}
					} catch (err) {
						// if no results where found. This happens when using delete or update.

						if (results.affectedRows <= 0) {
							res.status(error_statuses.STATUS_404).json({
								result: "No rows deleted",
							})
						} else {
							// if returning after a delete or update
							res.status(200).json({
								result: results.affectedRows + " affected",
							})
						}
					}
				}
			})
		}
	},

	executeUpdateQuery(
		err,
		connection,
		res,
		sqlQuery,
		errorMessage,
		errorStatus
	) {
		logger.log("Called executeUpdateQuery")
		if (err) {
			res.status(errorStatus).json({
				message: errorMessage,
				error: err,
			})
		}
		if (connection) {
			connection.query(sqlQuery, (error, results, fields) => {
				connection.release()
				if (error) {
					res.status(errorStatus).json({
						message: errorMessage,
						error: error,
					})
				}
				logger.log("resullts" + results)
				if (results) {
					loggerLog.trace("results: ", results)

					try {
						// if getting
						const mappedResults = results.map((item) => {
							return {
								...item,
								user: {
									name:
										item.First_Name + " " + item.Last_Name,
								},
							}
						})

						try {
							// check if we updated anything
							if (results[0].affectedRows == 0) {
								// if not,
								// did it fail because no resultls where found
								if(results[1] == 0){
									res.status(error_statuses.STATUS_404).json({
										result: "No results found/impacted",
									})
								} else{
									// did it fail because no AUTHORITY
									res.status(error_statuses.STATUS_401).json({
										result: "None found or you do not have acces",
									})
								}
							} else{
								// if everything succeeded
								res.status(200).json({
									result: mappedResults,
								})
							}				
						} catch (err) {
							// this usually never happens.
							if (mappedResults.length > 0) {
								res.status(200).json({
									result: mappedResults,
								})
							} else {
								// if no results are found, return 404
								res.status(error_statuses.STATUS_401).json({
									result: "None found or you do not have acces",
								})
							}
						}
					} catch (err) {
						if (results.affectedRows <= 0) {
							res.status(error_statuses.STATUS_404).json({
								result: "No rows deleted",
							})
						} else {
							// if returning after a delete or update
							res.status(200).json({
								result: results.affectedRows + " affected",
							})
						}
					}
				}
			})
		}
	},

	executeDeleteQuery(
		err,
		connection,
		res,
		sqlQuery,
		errorMessage,
		errorStatus
	) {
		logger.log("Called executeDeleteQuery")
		if (err) {
			res.status(errorStatus).json({
				message: errorMessage,
				error: err,
			})
		}
		if (connection) {
			connection.query(sqlQuery, (error, results, fields) => {
				connection.release()
				if (error) {
					res.status(errorStatus).json({
						message: errorMessage,
						error: error,
					})
				}
				if (results) {
					loggerLog.trace("results: ", results)

					try {
						// if getting
						const mappedResults = results.map((item) => {
							return {
								...item,
								user: {
									name:
										item.First_Name + " " + item.Last_Name,
								},
							}
						})

						try {
							
							if (results[0].affectedRows == 0) {
								// did it fail because no resultls where found
								if(results[1].length == 0){
									res.status(error_statuses.STATUS_404).json({
										result: "No results found/impacted",
									})
								} else{
									// did it fail because no AUTHORITY
									res.status(error_statuses.STATUS_401).json({
										result: "None found or you do not have acces",
									})
								}
							} else {
								// if delete
								res.status(200).json({
									result: mappedResults,
								})
							}
						} catch (err) {
							if (mappedResults.length > 0) {
								res.status(200).json({
									result: mappedResults,
								})
							} else {
								// if no results are found, return 404
								res.status(error_statuses.STATUS_401).json({
									result: "None found or you do not have acces",
								})
							}
						}
					} catch (err) {
						if (results.affectedRows <= 0) {
							res.status(error_statuses.STATUS_404).json({
								result: "No rows deleted",
							})
						} else {
							// if returning after a delete or update
							res.status(200).json({
								result: results.affectedRows + " affected",
							})
						}
					}
				}
			})
		}
	},

	executeDeleteStudenthomeQuery(
		err,
		connection,
		res,
		sqlQuery,
		errorMessage,
		errorStatus
	) {
		logger.log("Called executeDeleteStudenthomeQuery")
		if (err) {
			res.status(errorStatus).json({
				message: errorMessage,
				error: err,
			})
		}
		if (connection) {
			connection.query(sqlQuery, (error, results, fields) => {
				connection.release()
				if (error) {
					res.status(errorStatus).json({
						message: errorMessage,
						error: error,
					})
				}
				if (results) {
					loggerLog.trace("results: ", results)
					try {
						// if getting
						const mappedResults = results.map((item) => {
							return {
								...item,
							}
						})

						try {
							if (results[0].affectedRows == 0 && results[1].affectedRows == 0) {
								// did it fail because no resultls where found
								if(results[2] == 0){
									res.status(error_statuses.STATUS_404).json({
										result: "No results found/impacted",
									})
								} else{
									// did it fail because no AUTHORITY
									res.status(error_statuses.STATUS_401).json({
										result: "None found or you do not have acces",
									})
								}
							} else{
								res.status(200).json({
									result: mappedResults,
								})
							}							
						} catch (err) {
							if (mappedResults.length > 0) {
								res.status(200).json({
									result: mappedResults,
								})
							} else {
								// if no results are found, return 404
								res.status(error_statuses.STATUS_401).json({
									result: "None found or you do not have acces",
								})
							}
						}
					} catch (err) {
						if (results.affectedRows <= 0) {
							res.status(error_statuses.STATUS_404).json({
								result: "No rows deleted",
							})
						} else {
							// if returning after a delete or update
							res.status(200).json({
								result: results.affectedRows + " affected",
							})
						}
					}
				}
			})
		}
	},
}
