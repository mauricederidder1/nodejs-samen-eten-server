var logger = require("tracer").console()

const express = require("express")
const studentHomeController = require("../controllers/studenthomeController")
const router = express.Router()
const authcontroller = require("../controllers/authentication.controller")
// Beheer studenten huis UC 201 tot 206

logger.log("Called class studenthome")

// 201
router.post(
	"/",
	authcontroller.validateToken,
	studentHomeController.validateStudenthome,
	studentHomeController.create
)
// 202
router.get("/", studentHomeController.findHomesByCity)
// 203
router.get("/:homeId", studentHomeController.findHomeById)
// 204
router.put(
	"/:homeId",
	authcontroller.validateToken,
	studentHomeController.validateStudenthome,
	studentHomeController.changeHomeById
)
// 205
router.delete(
	"/:homeId",
	authcontroller.validateToken,
	studentHomeController.deleteHome
)
// 206
// router.get('/:homeId/userDe',studenthomeController)

module.exports = router
