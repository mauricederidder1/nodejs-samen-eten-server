const assert = require("assert")

var logger = require("tracer").console()
const studentHomeSql = require("../dao/sqlStudentHomeDao")
const errorStatus = require("./error_statuses")

module.exports = {
	// Every controller gets called by a route
	// Every controller calls a DAO method and passes on error data and the req,res
	validateStudenthome: (req, res, next) => {
		console.log("studenthomeController.Validate called")

		try {
			const { cityName, houseName, postalCode, phoneNumber } = req.body

			assert(typeof cityName === "string", "cityName is missing!")
			assert(typeof houseName === "string", "houseName is missing!")
			assert(typeof postalCode === "string", "postalCode is missing!")
			assert(typeof phoneNumber === "string", "phoneNumber is missing!")

			console.log("Studenthome data is valid")

			next()
		} catch (err) {
			console.log("Studenthome data is invalid: ", err.message)
			res.status(400)
			res.send(err.message)
		}
	},

	create: (req, res, next) => {
		logger.log("studenthomeController.Create called!")

		const errorMessage = "Failed calling query!"

		studentHomeSql.createHome(
			req,
			res,
			errorMessage,
			errorStatus.STATUS_400
		)
	},

	findHomesByCity: (req, res, next) => {
		logger.log("studenthomeController.findHomesByCity called!")

		const errorMessage = "Failed calling query!"

		studentHomeSql.getByCity(req, res, errorMessage, errorStatus.STATUS_404)
	},

	changeHomeById: (req, res, next) => {
		logger.log("studenthomeController.changeHomeById called!")

		const errorMessage = "Failed calling query!"

		studentHomeSql.changeHomeById(
			req,
			res,
			errorMessage,
			errorStatus.STATUS_404
		)
	},

	findHomeById: (req, res, next) => {
		logger.log("studenthomeController.findHomesById called!")

		const homeId = req.params.homeId

		const errorMessage = "Failed calling query!"

		studentHomeSql.getById(
			req,
			res,
			homeId,
			errorMessage,
			errorStatus.STATUS_404
		)
	},

	deleteHome: (req, res, next) => {
		logger.log("studenthomeController.deleteHome called!")

		const homeId = req.params.homeId

		const errorMessage = "Failed calling query!"

		studentHomeSql.deleteById(
			req,
			res,
			homeId,
			errorMessage,
			errorStatus.STATUS_404
		)
	},
}
