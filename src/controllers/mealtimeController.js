const assert = require("assert")

const config = require("../config/config")
const loggerLog = config.logger

var logger = require("tracer").console()
const database = require("../config/database")
const pool = require("../config/database")
const sqlMealtimeDao = require("../dao/sqlMealtimeDao")
const errorStatus = require("./error_statuses")

module.exports = {
	validateMeal: (req, res, next) => {
		logger.log("mealtimeController.validateMeal called!")

		try {
			const {
				name,
				description,
				ingredients,
				allergies,
				price,
				maxParticipants,
			} = req.body

			assert(typeof name === "string", "name is missing!")
			assert(typeof description === "string", "description is missing!")
			assert(typeof ingredients === "string", "ingredients is missing!")
			assert(typeof allergies === "string", "allergies is missing!")
			assert(typeof price === "number", "price is missing!")
			assert(
				typeof maxParticipants === "number",
				"maxParticipants is missing!"
			)

			console.log("Meal data is valid")

			next()
		} catch (err) {
			console.log("Meal data is invalid: ", err.message)
			res.status(400)
			res.send(err.message)
		}
	},
	createMeal: (req, res, next) => {
		logger.log("mealtimeController.createMeal called!")

		const errorMessage = "Failed calling query!"

		sqlMealtimeDao.createMeal(
			req,
			res,
			errorMessage,
			errorStatus.STATUS_400
		)
	},
	// TODO:: deze word nu niet aangeroepen omdat de route hetzelfde is als de getByStudentHomeId van studenthome
	findMealByHomeId: (req, res, next) => {
		logger.log("mealtimeController.findMealByHomeId called!")

		const errorMessage = "Failed calling query!"

		sqlMealtimeDao.getByStudentHomeId(
			req,
			res,
			errorMessage,
			errorStatus.STATUS_400
		)
	},

	findMealByMealId: (req, res, next) => {
		logger.log("mealtimeController.findMealByMealId called!")

		const errorMessage = "Failed calling query!"

		sqlMealtimeDao.getByMealIdAndHomeId(
			req,
			res,
			errorMessage,
			errorStatus.STATUS_400
		)
	},

	deleteMeal: (req, res, next) => {
		logger.log("mealtimeController.deleteMeal called!")

		const errorMessage = "Failed calling query!"

		sqlMealtimeDao.deleteById(
			req,
			res,
			errorMessage,
			errorStatus.STATUS_400
		)
	},

	changeMeal: (req, res, next) => {
		logger.log("mealtimeController.changeMeal called!")

		const errorMessage = "Failed calling query!"

		sqlMealtimeDao.changeMealById(
			req,
			res,
			errorMessage,
			errorStatus.STATUS_400
		)
	},
}
