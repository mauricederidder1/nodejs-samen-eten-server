const assert = require("assert")

const config = require("../config/config")
const loggerLog = config.logger

var logger = require("tracer").console()
const database = require("../config/database")
const pool = require("../config/database")
const { create } = require("../controllers/studenthomeController")

const databaseExecutor = require("./sqlExecute")

module.exports = {
	// Every DAO calls a method from sqlExecute.js
	// Every sqlDAO is responsible for making the SQL query
	deleteById(req, res, errorMessage, errorStatus) {
		const mealId = req.params.mealId

		const sqlQuery =
			"DELETE " +
			"FROM meal" +
			" WHERE ID = " +
			mealId +
			// Check if the user has the authority to delete the meal
			" AND UserID = " +
			req.userId +
			"; " +
			// check if the meal exists (more on this in sqlExecute)
			"SELECT * FROM meal WHERE StudenthomeID = " +
			req.params.homeId +
			" AND ID = " +
			req.params.mealId +
			";"

		pool.getConnection(function (err, connection) {
			databaseExecutor.executeDeleteQuery(
				err,
				connection,
				res,
				sqlQuery,
				errorMessage,
				errorStatus
			)
		})
	},

	getByMealIdAndHomeId(req, res, errorMessage, errorStatus) {
		const mealId = req.params.mealId
		const homeId = req.params.homeId

		const sqlQuery =
			"SELECT * " +
			"FROM meal" +
			" WHERE ID = " +
			mealId +
			" AND StudentHomeID = " +
			homeId

		pool.getConnection(function (err, connection) {
			databaseExecutor.executeQuery(
				err,
				connection,
				res,
				sqlQuery,
				errorMessage,
				errorStatus
			)
		})
	},

	getByStudentHomeId(req, res, errorMessage, errorStatus) {
		const sqlQuery =
			"SELECT * " +
			"FROM meal " +
			" WHERE StudenthomeID = " +
			req.params.homeId

		pool.getConnection(function (err, connection) {
			databaseExecutor.executeQuery(
				err,
				connection,
				res,
				sqlQuery,
				errorMessage,
				errorStatus
			)
		})
	},

	changeMealById(req, res, errorMessage, errorStatus) {
		const meal = req.body

		let {
			name,
			description,
			ingredients,
			allergies,
			price,
			maxParticipants,
		} = meal

		const sqlQuery =
			"UPDATE meal SET `Name` = " +
			"'" +
			name +
			"' " +
			", `Description` = " +
			"'" +
			description +
			"' " +
			", `Ingredients` = " +
			"'" +
			ingredients +
			"' " +
			", `Allergies` = " +
			"'" +
			allergies +
			"' " +
			", `Price` = " +
			"'" +
			price +
			"' " +
			", `MaxParticipants` = " +
			"'" +
			maxParticipants +
			"' " +
			"WHERE ID = " +
			req.params.mealId +
			" AND StudenthomeID = " +
			req.params.homeId +
			// Check if the user has the authority to change this meal
			" AND UserID = " +
			req.userId +
			"; " +
			// check if the meal exists
			"SELECT * FROM meal WHERE StudenthomeID = " +
			req.params.homeId +
			" AND ID = " +
			req.params.mealId +
			";"

		pool.getConnection(function (err, connection) {
			databaseExecutor.executeUpdateQuery(
				err,
				connection,
				res,
				sqlQuery,
				errorMessage,
				errorStatus
			)
		})
	},
	createMeal(req, res, errorMessage, errorStatus) {
		// This create function does not call a method from the sqlExecute class because of the code on line 182
		const meal = req.body
		let {
			name,
			description,
			ingredients,
			allergies,
			price,
			maxParticipants,
		} = meal

		let createdOn = new Date().toLocaleString()
		let offeredOn = new Date().toLocaleString()
		let studentHomeId = req.params.homeId
		const sqlQuery =
			"INSERT INTO `meal`(`Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES (?,?,?,?,?,?,?,?,?,?)"

		pool.getConnection(function (err, connection) {
			if (err) {
				logger.error("createMeal", error)
				res.status(errorStatus).json({
					message: errorMessage,
					error: err,
				})
			}

			if (connection) {
				// This code \/
				connection.query(
					sqlQuery,
					[
						name,
						description,
						ingredients,
						allergies,
						createdOn,
						offeredOn,
						price,
						req.userId,
						studentHomeId,
						maxParticipants,
					],
					(error, results, fields) => {
						// When done with the connection, release it.
						connection.release()
						// Handle error after the release.
						if (error) {
							logger.error("mealtime", error.toString())
							res.status(errorStatus).json({
								message: errorMessage,
								error: error.toString(),
							})
						}
						if (results) {
							logger.trace("results: ", results)

							res.status(200).json({
								result: {
									id: results.insertId,
									...meal,
								},
							})
						}
					}
				)
			}
		})
	},
}
